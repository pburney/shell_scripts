#!/bin/bash
# This simple script backs up the directories from my home directory to the box directory which is backed up offsite
DESTINATION_DIRECTORY="Box Sync/Backups"
BACKUP_ITEMS=("Desktop" "Documents" "Library/Preferences" "Library/Keychains" "Library/Application Support" "Pictures" "bin" ".zsh_history" ".zshrc" ".viminfo" ".ssh" ".oh-my-zsh" ".my.cnf" ".login" ".iterm2" ".gitconfig")
IS_VERBOSE=""

# We don't need fancy getopts here, keep it simple
if [ "$1" = "-v" ]
then
  IS_VERBOSE="true"
fi

for BACKUP_ITEM in "${BACKUP_ITEMS[@]}"; do
  if [ "$IS_VERBOSE" = "true" ]
  then
    echo "Backing up ${HOME}/${BACKUP_ITEM} to ${HOME}/${DESTINATION_DIRECTORY}"
    rsync -avr "${HOME}/${BACKUP_ITEM}" "${HOME}/${DESTINATION_DIRECTORY}"
  else
    # We drop the -v here to be quiet and not generate messages
    rsync -ar "${HOME}/${BACKUP_ITEM}" "${HOME}/${DESTINATION_DIRECTORY}"
  fi
done
